import UIKit
import Foundation
import os
import SwiftUI

class MainViewController: UIViewController {
    
    let mainView: UIView = {
        let uiView = MainView()
        uiView.translatesAutoresizingMaskIntoConstraints = false
        return uiView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(mainView)
        view.backgroundColor = .red
        mainView.topAnchor.constraint(equalTo: view.topAnchor, constant: 32).isActive = true
        mainView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        mainView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
    }
}

#if DEBUG
@available(iOS 13.0, *)
struct MainViewControllerRepresentable: UIViewControllerRepresentable {
    func makeUIViewController(context: UIViewControllerRepresentableContext<MainViewControllerRepresentable>) -> MainViewController {
        return MainViewController()
    }
    
    func updateUIViewController(_ uiViewController: MainViewController, context: UIViewControllerRepresentableContext<MainViewControllerRepresentable>) {
    }
    
}

@available(iOS 13.0, *)
struct MainViewController_Preview: PreviewProvider {
    static var previews: some View {
        Group {
            MainViewControllerRepresentable()
                .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
                .previewDisplayName("iPhone SE")
            
            MainViewControllerRepresentable()
                .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
                .previewDisplayName("iPhone XS Max")
            
            MainViewControllerRepresentable()
                .previewDisplayName("iPhone 8")
                .previewDevice(PreviewDevice(rawValue: "iPhone 8"))
            
            MainViewControllerRepresentable()
                .previewDisplayName("iPhone 8 LandScape")
                .previewDevice(PreviewDevice(rawValue: "iPhone 8"))
                .previewLayout(.fixed(width: 667, height: 375))
        }
        
    }
}
#endif
