import UIKit
import Foundation
import SwiftUI

class MainView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initView()
    }
    
    func initView() {
        self.backgroundColor = .green
    }
}

#if DEBUG
@available(iOS 13.0, *)
struct MainViewRepresentable: UIViewRepresentable {
    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<MainViewRepresentable>) {
        
    }
    
    func makeUIView(context: Context) -> UIView {
        return MainView()
    }
}

@available(iOS 13.0, *)
struct MainView_Preview: PreviewProvider {
    static var previews: some View {
        MainViewRepresentable()
    }
}

@available(iOS 13.0, *)
struct MainViewLandScape_Preview: PreviewProvider {
    static var previews: some View {
//        The fixed size won't respect safe areas and doesn't show correctly status, navigation, tab bar, and master/detail views (on Ipad) properly. 
        MainViewRepresentable()
            .previewDisplayName("iPhone 8 LandScape")
            .previewDevice(PreviewDevice(rawValue: "iPhone 8"))
            .previewLayout(.fixed(width: 667, height: 375))
    }
}
#endif
